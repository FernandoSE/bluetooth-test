package com.example.testebluetooth;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MainActivity extends Activity {
	private static final int REQUEST_ENABLE_BT = 1;
	String tag = "TesteBluetooth";
	Button discoverButton;
	Button discoverBtButton;
	
	ArrayList<String> mArrayList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mArrayList = new ArrayList<String>();
		
		discoverButton = (Button) findViewById(R.id.activateBtButton);
		discoverButton.setOnClickListener(activateBtButtonListener);
		
		discoverBtButton = (Button) findViewById(R.id.discoverBtButton);
		discoverBtButton.setOnClickListener(discoverButtonClickListener);

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter.isEnabled()) {
			discoverBtButton = (Button) findViewById(R.id.discoverBtButton);
			discoverBtButton.setClickable(true);
			discoverBtButton.setEnabled(true);
		}
				
	}
	
	OnClickListener activateBtButtonListener = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if(mBluetoothAdapter == null) {
				Log.d(tag, "Bluetooth not supported in this device");
			}
			if(!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
			if(mBluetoothAdapter.isEnabled()) {
				Button discoverBtButton = (Button) findViewById(R.id.discoverBtButton);
				discoverBtButton.setClickable(true);
				discoverBtButton.setEnabled(true);
			}
		}
	};
	
	OnClickListener discoverButtonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
			registerReceiver(mReceiver, filter);
			
			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			mBluetoothAdapter.startDiscovery();
		}
	};
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				mArrayList.add(device.getName() + "\n" + device.getAddress());
				
				ListView lstDevices = (ListView) findViewById(R.id.lstDevices);
				
				ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, mArrayList);
				
				lstDevices.setAdapter(mAdapter);
				
				lstDevices.setOnItemClickListener(lstDevicesItemClickListener);
				
				//lstDevices.setAdapter();
				
				//lstDevices;
			}
		}
	};
	
	OnItemClickListener lstDevicesItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Log.d(tag, "position: "+position);
			String item = (String) parent.getItemAtPosition(position);
			int indexLineBreak = item.indexOf('\n');
			
			String nome = item.substring(0, indexLineBreak);
			String mac = item.substring(indexLineBreak+1);
			
			Intent abrirActivicty = new Intent(view.getContext(), DeviceActivity.class);
			
			//Bundle extras = abrirAtivicty.getExtras();
			
			/*extras.putString("nome", item);
			extras.putString("nome", nome);
			extras.putString("mac", mac);*/
			
			abrirActivicty.putExtra("nome", nome);
			abrirActivicty.putExtra("mac", mac);
			
			//abrirActivicty.putString("nome", "ABC");
			//abrirActivicty.putString("mac", "MAC");
			
			startActivity(abrirActivicty);
			
		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
			Button discoverBtButton = (Button) findViewById(R.id.discoverBtButton);
			discoverBtButton.setClickable(true);
			discoverBtButton.setEnabled(true);
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
